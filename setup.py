from setuptools import setup, find_packages


setup(
    name="bookparser",
    version="0.1",
    packages=find_packages(),
)
