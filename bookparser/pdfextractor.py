from __future__ import unicode_literals, print_function
import spacy
from spacy.lang.en import English


import pdfplumber
import re



class PDF:
    def __init__(self, fileobj):
        if type(fileobj) is str:
            self.pdf = pdfplumber.open(fileobj, password=b"")
        else:
            try:
                self.pdf = pdfplumber.PDF(fileobj, password=b"")
            except AttributeError:
                self.pdf = pdfplumber.PDF(fileobj, password="")
            
        self.nlp = English()
        sentencizer = self.nlp.create_pipe('sentencizer')
        sentencizer.punct_chars.union(set(['.”']))
        self.nlp.add_pipe(sentencizer)

    def get_page(self, number, remove_headers=True):
        page = self.pdf.pages[number]
        if remove_headers:
            page = page.filter(lambda obj: (obj['object_type']=='char' and obj['top'] > 90 and obj['y0'] > 70))
        text = page.extract_text(y_tolerance=12, x_tolerance=15)
        return text

    def text_cleaning(self, text):

        text=text.strip()

        # If first letter (of a chapter) was badly separated, merge it
        text = re.sub('(?<=\n[A-Z])\s(?=[a-z])', '', text)#, flags=re.MULTILINE)

        #If there is a linebreak+letter, thats a space
        text = re.sub('(?<=[a-z])\n(?=[a-z])', ' ', text)

        # Merge lines splitted by a -
        text = re.sub('(?<=[a-zA-Z])\s*[-]\s*\n+(?=[a-z])', '', text)

        # We dont need linebreaks anymore
        #text = text.replace('.\n',' ')
        text = text.replace('\n',' ')

        # We dont want tabs
        text = text.replace('\t', ' ')

        # If something has several spaces  and it doesnt end with a . then it surely be
        # a separated phrase.
        # text = re.sub('\s{3,7}', '. ', text)


        # If something has lots of spaces it probably is on another line-
        text = re.sub('\s{6,}', '. ', text)

        text = re.sub('(?<=[A-Za-z])\s*\n+(?=[a-zA-Z])', ' ', text)

        text = re.sub('(?<=[\-,;])\s*\n+(?=[a-z])', ' ', text)


        # There could be . . . which equals ...
        text = text.replace(' . . .', '...')

        #Maybe we have inserted some -. on page ends
        text = re.sub('(?<=\w)-\.[\s](?=\Z)', '-', text)

        #Add space between quote end and quotestart
        text = re.sub('”(?=[A-Za-z])', '”. ', text)
        text = re.sub('[”“]', '', text)

        #ensure commas have an space after
        text = re.sub(',(?!=\s)', ', ', text)

        # Replace — with comma to match pause sound
        text = re.sub('(?<=[a-z])—', ', ', text)

        #its probable we have introduced some extra spaces so well remove them
        text = re.sub('\s{2,6}', ' ', text)

        return text.strip()

    def get_sents(self, text):
        doc = self.nlp(text)
        sentences = [sent.string.strip() for sent in doc.sents]
        sents_clean = []

        for sent in sentences:
            sent_s = sent.split('\n')
            for s in sent_s:
                if len(s) > 1:
                    sents_clean.append(s.strip())

        return sents_clean

    def extract_text(self, page_start, page_end, remove_headers=False):
        extr_phrases = []

        page_start -= 1
        page_end +=1

        text_pages = []
        for i in range(page_start, page_end):
            text = self.get_page(i, remove_headers=remove_headers)
            text = self.text_cleaning(text)
            text_pages.append(self.get_sents(text))

        # Merge phrases that are splitted by a newpage
        merged = False
        for i in range(len(text_pages) -1):
            currpage = text_pages[i]
            nextpage = text_pages[i+1]

            if merged:
                currpage = currpage[1:]   # If last page merged, then we dont need the first phrase
                merged = False

            if len(currpage) > 0:
                if re.search('([a-z0-9]$|,\s*$|-\s*$)', currpage[-1]): # IF last phrase doesn't end with a point
                    # We merge
                    currpage[-1] += ' ' + nextpage[0]
                    currpage[-1] = re.sub('\w-\.$', '', currpage[-1])
                    currpage[-1] = re.sub('(?<=[a-z])-\s(?=[a-z])', '-', currpage[-1])
                    merged = True
                else:
                    merged=False


            extr_phrases.extend(currpage)
        return extr_phrases




def load_page(filepath, pagenumber, remove_headers=True):
    with pdfplumber.open(filepath) as pdf:
        page = pdf.pages[pagenumber]
        if remove_headers:
            page = page.filter(lambda obj: (obj['object_type']=='char' and obj['top'] > 90 and obj['y0'] > 70))
        text = page.extract_text(y_tolerance=12, x_tolerance=15)
    return text




def text_cleaning(text):

    text=text.strip()

    # If first letter (of a chapter) was badly separated, merge it
    text = re.sub('(?<=\n[A-Z])\s(?=[a-z])', '', text)#, flags=re.MULTILINE)

    #If there is a linebreak+letter, thats a space
    text = re.sub('(?<=[a-z])\n(?=[a-z])', ' ', text)
    # We dont need linebreaks anymore
    #text = text.replace('.\n',' ')
    text = text.replace('\n',' ')

    # We dont want tabs
    text = text.replace('\t', ' ')

    # If something has several linebreaks and it doesnt end with a . then it surely be
    # a separated phrase.
    text = re.sub('\s{2,7}', '. ', text)


    # If something has lots of spaces it probably is on another line-
    text = re.sub('\s{6,}', '. ', text)

    text = re.sub('(?<=[A-Za-z])\s*\n+(?=[a-zA-Z])', ' ', text)

    text = re.sub('(?<=[\-,;])\s*\n+(?=[a-z])', ' ', text)

    # Merge lines splitted by a -
    text = re.sub('(?<=[a-zA-Z])\s*[-]\s*\n+(?=[a-z])', '', text)

    # There could be . . . which equals ...
    text = text.replace(' . . .', '...')

    #Maybe we have inserted some -. on page ends
    text = re.sub('(?<=\w)-\.[\s](?=\Z)', '-', text)

    #Add space between quote end and quotestart
    text = re.sub('”(?=[A-Za-z])', '”. ', text)
    text = re.sub('(”|“)', '', text)

    #ensure commas have an space after
    text = re.sub(',(?!=\s)', ', ', text)

    # Replace — with comma to match pause sound
    text = re.sub('(?<=[a-z])—', ', ', text)

    #its probable we have introduced some extra spaces so well remove them
    text = re.sub('\s{2,6}', ' ', text)

    return text.strip()

def get_sents(text):
    nlp = English()
    sentencizer = nlp.create_pipe('sentencizer')
    sentencizer.punct_chars.union(set(['.”']))
    nlp.add_pipe(sentencizer)

    doc = nlp(text)
    sentences = [sent.string.strip() for sent in doc.sents]
    sents_clean = []

    for sent in sentences:
        sent_s = sent.split('\n')
        for s in sent_s:
            if len(s) > 1:
                sents_clean.append(s.strip())

    return sents_clean



def extract_phrases(page_start, page_end, pdf, remove_headers=False):
    extr_phrases = []

    page_start -= 1
    page_end +=1

    text_pages = []
    for i in range(page_start, page_end):
        text = load_page(pdf, i, remove_headers=remove_headers)
        text = text_cleaning(text)
        text_pages.append(get_sents(text))

    # Merge phrases that are splitted by a newpage
    merged = False
    for i in range(len(text_pages) -1):
        currpage = text_pages[i]
        nextpage = text_pages[i+1]

        if merged:
            currpage = currpage[1:]   # If last page merged, then we dont need the first phrase
            merged = False

        if len(currpage) > 0:
            if re.search('([a-z0-9]$|,\s*$|-\s*$)', currpage[-1]): # IF last phrase doesn't end with a point
                # We merge
                currpage[-1] += ' ' + nextpage[0]
                currpage[-1] = re.sub('\w-\.$', '', currpage[-1])
                currpage[-1] = re.sub('(?<=[a-z])-\s(?=[a-z])', '-', currpage[-1])
                merged = True
            else:
                merged=False


        extr_phrases.extend(currpage)

    return extr_phrases

