import unittest
import json
import os
import warnings

from bookparser import pdfextractor

RES_PATH = (os.path.dirname(__file__)) + "/res/"

with open(RES_PATH+"expected_results.json") as file:
    right_results = json.loads(file.read())

class TestPdfExtractor(unittest.TestCase):

    def setUp(self):
            warnings.simplefilter("ignore", ResourceWarning)
    def tearDown(self):
        warnings.simplefilter("default", ResourceWarning)

    def test_extract_without_header(self):

        pdf = pdfextractor.PDF(RES_PATH + "ahsoka.pdf")
        res = pdf.extract_text(1, 4, remove_headers=False)
        self.assertTrue(res[0] == right_results['ahsoka'][0])
        self.assertTrue(res[-1] == right_results['ahsoka'][-1])
        self.assertTrue(len(res) == len(right_results['ahsoka']))



    def test_extract_with_header(self):
        pdf = pdfextractor.PDF(RES_PATH + "harrypotter.pdf")
        res = pdf.extract_text(1, 4, remove_headers=True)
        self.assertTrue(res[0] == right_results['harry'][0])
        self.assertTrue(res[-1] == right_results['harry'][-1])
        self.assertTrue(len(res) == len(right_results['harry']))


if __name__ == "__main__":
    unittest.main()
